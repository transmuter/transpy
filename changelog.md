# 2019/06/27

- Optimizations to data orchestration for passing to Transmuter, to reduce risk
  of memory leaks, which are exacerbated by larger benchmarks.
  
- Move mm from ATen/LinearAlgebraOps.cpp to TH/generic/THBlas.cpp in order to
  fix memory problems relating to incorrect Tensor type detection 

- Fix conv packaging in torch to stop calling incorrect version of conv2d 

- Fix multiply alias in scipy, which was calling dot instead of element-wise
  multiplication, causing incorrect results
  
- Fix packaging issue in with pytorch where attempting to import certain
  subpackages caused import errors
  
- Fix related but different issue where top-level modues would be bound to the
  names of submodules (e.g. `transpy.torch.nn` would resolve to `torch`). This
  only really manifested when using the `from foo.baz import bar` form




# 2019/06/16
- Fixed issue where some non-accelerated numpy, scipy and torch modules would not
fail over to the default upstream version properly.

- Fixed various memory leaks in accelerated code and python interface

- General performance improvements

- Reduced size of Docker container and optimised build times

- Fixed issue where `backward` function in PyTorch could produce incorrect results
