#!/usr/bin/env python3

import transpy.numpy as tp

a = tp.random.rand(8,7)
b = tp.random.rand(7,8)

print(a.dot(b))
