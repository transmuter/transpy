- Fixed issue where some non-accelerated numpy, scipy and torch modules would not
fail over to the default upstream version properly.

- Fixed various memory leaks in accelerated code and python interface

- General performance improvements

- Reduced size of Docker container and optimised build times

- Fixed issue where `backward` function in PyTorch could produce incorrect results
