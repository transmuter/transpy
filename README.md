# transpy

Transmuter Python software environment

**For detailed documentation, please see https://transpy.readthedocs.io/en/latest/**

Note: depending on your system, you may have to `sudo` all docker commands

# Building the docker image

First make sure you're logged-in to the docker registry.  
Run `docker login registry.sdh.cloud`

Simply run the script `docker_build.sh`. This will build an image tagged
'transpy'.

You can also build the image in the usual way from the Dockerfile using `docker
build --no-cache --pull -t transpy . ` or pull it by using `docker pull
registry.sdh.cloud/2019-06-submissions/michigansdhph1/transpy`



# Runnning programs in the docker container
The `docker_python.sh` script runs a local python program passed as the first
argument in the container, and stops and removes the container afterwards. Using
the supplied `example.py` program (this performs basic matrix multiplation), we
can run it simply by executing `./docker_python.sh example.py`. We should then
see the following output: ![screenshot](./screenshot.png)

Programs with more complicated arguments should probably be run as follows:

`docker run -it --rm --name transmuter-script -v "$PWD":/data -w
/data transpy python <your_args> <script_name>`

The `-v` flag here copies the current working directory on the host to the
`/data` directory in the container; this approach is useful for getting test
data into the system (note that this is what the `docker_python.sh` script does
too).

The `--rm` flag removes the container after the process exits

Please see the [docker
documentation](https://docs.docker.com/engine/reference/run/) for more details.

# Accessing the docker container directly

## Shell
If you want to run programs unside the docker container, you can run `docker run
-it --rm --name transmuter-bash -v "$PWD":/data -w /data
transpy  bash` (or just run `docker_shell.sh`). This will open an interactive shell running as root in the
container.

## ssh

Note: the 'Shell' method is strongly recommended over the ssh method.

There is an open-ssh server running on the container First, we run the container
persistently e.g.

`docker run -d -P --name transpy_sshd transpy`

Then we can get the port that the container's port 22 is forwared to:

`docker port transpy_sshd 22`


Which will ouput something like `0.0.0.0:12345`

we can then `ssh root@0.0.0.0 -p 12345`. The root password is `root`


# External dependencies/ Hardware requirements

In current release, Transpy runs on any Intel x86_64 machines with Ubuntu 16.04 operating system.

Note: For accurate energy and performance measurements, the container should
be run on a machine with minimal load from other programs or services.


